<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalAsignaturas = Subject::all();
        $asignaturas = Subject::orderBy('id', 'asc')->paginate(5);

        return view('crud.asignaturas.index', compact('totalAsignaturas', 'asignaturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::all();
        
        return view('crud.asignaturas.create', compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "nombre" => ['required', 'string', 'min:1', 'max:40'],
        ]);

        $subject = new Subject();

        $subject->nombre = request('nombre');
        $subject->profesor_id = request('profesor');
        $subject->save();

        return redirect()->route('subjects.index')->with('mensaje', "Asignatura creada con éxito.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return view('crud.asignaturas.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $teachers = Teacher::all();
        
        return view('crud.asignaturas.edit', compact('subject', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        $request->validate([
            "nombre" => ['required', 'string', 'min:1', 'max:40'],
        ]);

        $subject = Subject::findOrFail($subject->id);

        $subject->nombre = $request->get('nombre');

        $subject->profesor_id = $request->get('profesor');

        $subject->update($request->only('nombre', 'profesor'));

        return redirect()->route('subjects.index')->with('mensaje', "Asignatura actualizada con éxito.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();
        return redirect('/subjects')->with('mensaje', 'Asignatura borrada con éxito.');
    }
}
