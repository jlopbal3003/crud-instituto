<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalProfesores = Teacher::all();
        $profesores = Teacher::orderBy('id', 'asc')->paginate(5);
        $arrayProfesores = [];

        foreach ($profesores as $profesor) {
            $asignaturasProfesor = Subject::where('profesor_id', $profesor->id)->get();
        }

        return view('crud.profesores.index', compact('totalProfesores', 'profesores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::whereNull('profesor_id')->get();
        
        return view('crud.profesores.create', compact('subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'avatar' => 'mimes:jpeg,png,jpg|max:2048|dimensions:min_width=138,min_height=138',
            "nombre" => ['required', 'string', 'min:1', 'max:40'],
            "apellidos" => ['required', 'string', 'min:1', 'max:40'],
            "email" => ['required', 'unique:teachers,email'],
        ]);

        $teacher = new Teacher();

        //Si se ha subido un archivo
        if ($request->hasFile('avatar')) {
            $teacher->avatar = $request->file('avatar')->store('public');
        }

        $teacher->nombre = request('nombre');
        $teacher->apellidos = request('apellidos');
        $teacher->email = request('email');
        $teacher->save();

        $subjects = $request->asignaturas;

        foreach ($subjects as $subject) {
            $asignatura = Subject::findOrFail($subject);
            
            if($asignatura) {
                $asignatura->profesor_id = $teacher->id;
                $asignatura->save();
            }
        }

        return redirect()->route('teachers.index')->with('mensaje', "Profesor creado con éxito.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        $subjects = Subject::where('profesor_id', $teacher->id)->get();
        
        return view('crud.profesores.show', compact('teacher', 'subjects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('crud.profesores.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $request->validate([
            'avatar' => 'mimes:jpeg,png,jpg|max:2048|dimensions:min_width=138,min_height=138',
            "nombre" => ['required', 'string', 'min:1', 'max:40'],
            "apellidos" => ['required', 'string', 'min:1', 'max:40'],
            "email" => ['required', 'unique:teachers,email,'.$teacher->id],
        ]);

        $teacher = Teacher::findOrFail($teacher->id);

        //Si se ha subido un archivo
        if ($request->hasFile('avatar')) {
            $teacher->avatar = $request->file('avatar')->store('public');
        }

        $teacher->nombre = $request->get('nombre');

        $teacher->apellidos = $request->get('apellidos');

        $teacher->email = $request->get('email');

        $teacher->update($request->only('nombre', 'apellidos', 'email'));

        return redirect()->route('teachers.index')->with('mensaje', "Profesor actualizado con éxito.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $subjects = Subject::where('profesor_id', $teacher->id)->get();

        foreach ($subjects as $subject) {
            $subject->profesor_id = null;
            $subject->save();
        }
        
        $teacher->delete();
        return redirect('/teachers')->with('mensaje', 'Profesor borrado con éxito.');
    }
}
