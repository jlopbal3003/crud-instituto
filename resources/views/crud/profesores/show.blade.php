@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Detalles del profesor</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('teachers.index')}}" type="button" class="btn btn-secondary mb-3"><i class="fa-solid fa-arrow-left"></i></a></div>
                    <div class="float-end">
                        <a href="{{route('teachers.edit', $teacher)}}" class="btn btn-warning me-1"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
                        <form id="formBorrar" class="d-inline" action="{{route('teachers.destroy', $teacher)}}" method="POST">
                            @csrf
                            @method("DELETE")
                            <button type="submit" class="btn btn-danger d-none"><i class="fas fa-trash"></i></button>
                        </form>
                            <button class="btn btn-danger" onclick=
                            "Swal.fire({
                                title: '¿Desea eliminar al profesor \u201C{{$teacher->nombre}}\u201D?',
                                showConfirmButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Eliminar',
                                cancelButtonText: `Cancelar`,
                                }).then((result) => {
                                if (result.isConfirmed) {
                                    document.getElementById('formBorrar').submit();
                                }
                            })"
                            ><i class="fas fa-trash"></i> Eliminar</button>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-6 d-flex justify-content-center align-items-center mb-5">
                        <div>
                            @if ($teacher->avatar === null)
                                <img width="265px" height="330px" src="https://i.ibb.co/Cmp8Xr7/nopic.png"/>
                            @else
                                <img width="265px" height="330px" src="{{ Storage::url($teacher->avatar) }}" style="object-fit: cover;"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <div style="font-size: large;">
                            <p><b>Nombre:</b> {{$teacher->nombre}}</p>
                            <p><b>Apellidos:</b> {{$teacher->apellidos}}</p>
                            <p><b>Correo electrónico:</b> {{$teacher->email}}</p>
                            <p><b>Asignatura/s:</b> @foreach ($subjects as $key => $subject)@if ($key < sizeOf($subjects) - 1){{$subject->nombre}}, @else {{$subject->nombre}} @endif @endforeach</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
