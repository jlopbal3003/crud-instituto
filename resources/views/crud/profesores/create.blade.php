@extends('layouts.app')

@section('content')
<form action="{{ route('teachers.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Nuevo profesor</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('teachers.index')}}" type="button" class="btn btn-secondary mb-3"><i class="fa-solid fa-arrow-left"></i></a></div>
                    <div class="float-end">
                        <button type="submit" class="btn btn-primary me-1"><i class="fa-solid fa-plus"></i> Crear</button>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-6 d-flex justify-content-center align-items-center mb-5">
                        <div>
                            <p><img src="https://i.ibb.co/Cmp8Xr7/nopic.png"/></p>
                            <p><input type="file" id="inpAvatar" name="avatar" accept="image/png, image/jpeg"></p>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <div style="font-size: large;">
                            <div class="mb-3">
                                <label for="nombre" class="form-label"><b>Nombre:</b></label>
                                <input type="text" class="form-control" name="nombre">
                                @error('nombre')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="apellidos" class="form-label"><b>Apellidos:</b></label>
                                <input type="text" class="form-control" name="apellidos">
                                @error('apellidos')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label"><b>Correo electrónico:</b></label>
                                <input type="email" class="form-control" name="email">
                                @error('email')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="asignaturas" class="form-label"><b>Asignaturas:</b></label>
                                @if (count($subjects) > 1)
                                    @foreach ($subjects as $subject)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="asignaturas[]" value="{{$subject->id}}" id="flexCheckChecked">
                                            <label class="form-check-label" for="flexCheckChecked">
                                            {{$subject->nombre}}
                                            </label>
                                        </div>
                                    @endforeach
                                @else
                                <p><i>No hay asignaturas disponibles.</i></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
