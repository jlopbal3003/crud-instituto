@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Profesores ({{ count($totalProfesores) }})</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('teachers.create')}}" type="button" class="btn btn-primary mb-3"><i class="fa-solid fa-plus"></i> Nuevo</a></div>
                    <div class="float-end"><a href="{{route('subjects.index')}}" type="button" class="btn btn-warning"><i class="fa-solid fa-book-open"></i> Asignaturas</a></div>
                </div>
                @if (session('mensaje'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('mensaje') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="table-responsive">
                <table class="table rounded-3 overflow-hidden align-middle text-center">
                    <thead>
                        <tr class="table-dark">
                            <th scope="col"></th>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Correo electrónico</th>
                            <th scope="col">Asignaturas</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($profesores as $profesor)
                            @php $asignaturasProfesor = App\Models\Subject::where('profesor_id',$profesor->id)->get();@endphp
                            <tr @if (sizeOf($asignaturasProfesor) - 1 < 0) class="table-danger" @else class="table-success" @endif>
                                <th><a href="{{route('teachers.show', $profesor)}}" class="btn btn-success"><i class="fa-solid fa-info"></i></a></th>
                                <th scope="row">{{ $profesor->id }}</th>
                                <td>{{ $profesor->nombre }}</td>
                                <td>{{ $profesor->apellidos }}</td>
                                <td>{{ $profesor->email }}</td>
                                <td>
                                    @foreach ($asignaturasProfesor as $key => $asignatura)
                                        @if ($key < sizeOf($asignaturasProfesor) - 1)
                                            {{ $asignatura->nombre }},
                                        @else
                                            {{ $asignatura->nombre }}
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{route('teachers.edit', $profesor)}}" type="button" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></a>
                                    <form id="formBorrar{{$profesor->id}}" class="d-inline" action="{{route('teachers.destroy', $profesor)}}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger d-none"><i class="fas fa-trash"></i></button>
                                    </form>
                                    <button class="btn btn-danger" onclick=
                                    "Swal.fire({
                                        title: '¿Desea eliminar al profesor \u201C{{$profesor->nombre}}\u201D?',
                                        showConfirmButton: true,
                                        showCancelButton: true,
                                        confirmButtonText: 'Eliminar',
                                        cancelButtonText: `Cancelar`,
                                        }).then((result) => {
                                        if (result.isConfirmed) {
                                            document.getElementById('formBorrar{{$profesor->id}}').submit();
                                        }
                                    })"
                                    ><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                <div class="d-flex">
                    {!! $profesores->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
