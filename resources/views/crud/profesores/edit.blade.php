@extends('layouts.app')

@section('content')
<form action="{{ route('teachers.update', $teacher) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method("PUT")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Editar profesor</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('teachers.index')}}" type="button" class="btn btn-secondary mb-3"><i class="fa-solid fa-arrow-left"></i></a></div>
                    <div class="float-end">
                        <button type="submit" class="btn btn-primary me-1"><i class="fa-solid fa-floppy-disk"></i> Guardar cambios</button>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-6 d-flex justify-content-center align-items-center mb-5">
                        <div>
                            <p>
                            @if ($teacher->avatar === null)
                                <img width="265px" height="330px" src="https://i.ibb.co/Cmp8Xr7/nopic.png"/>
                            @else
                                <img width="265px" height="330px" src="{{ Storage::url($teacher->avatar) }}" style="object-fit: cover;"/>
                            @endif
                            </p>
                            <p><input type="file" id="inpAvatar" name="avatar" accept="image/png, image/jpeg"></p>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <div style="font-size: large;">
                            <div class="mb-3">
                                <label for="nombre" class="form-label">Nombre:</label>
                                <input type="text" class="form-control" name="nombre" value="{{$teacher->nombre}}">
                                @error('nombre')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="apellidos" class="form-label">Apellidos:</label>
                                <input type="text" class="form-control" name="apellidos" value="{{$teacher->apellidos}}">
                                @error('apellidos')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Correo electrónico:</label>
                                <input type="email" class="form-control" name="email" value="{{$teacher->email}}">
                                @error('correo')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
