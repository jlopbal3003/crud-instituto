@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('mensaje'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('mensaje') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <h1 class="pb-1"><i class="fa-solid fa-book-open"></i> Asignaturas ({{ count($totalAsignaturas) }})</h1>
                <div class="clearfix">
                <div class="float-start"><a href="{{route('subjects.create')}}" type="button" class="btn btn-primary mb-3"><i class="fa-solid fa-plus"></i> Nuevo</a></div>
                <div class="float-end"><a href="{{route('teachers.index')}}" type="button" class="btn btn-success"><i class="fa-solid fa-chalkboard-user"></i> Profesores</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table rounded-3 overflow-hidden align-middle text-center">
                        <thead>
                            <tr class="table-dark">
                                <th scope="col"></th>
                                <th scope="col">ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Profesor</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($asignaturas as $asignatura)
                                <tr @if ($asignatura->profesor_id === null) class="table-danger" @else class="table-warning" @endif>
                                    <th><a href="{{route('subjects.show', $asignatura)}}" class="btn btn-success"><i class="fa-solid fa-info"></i></a></th>
                                    <th scope="row">{{ $asignatura->id }}</th>
                                    <td>{{ $asignatura->nombre }}</td>
                                    <td>@if ($asignatura->profesor_id === null) @else {{ $asignatura->teacher->nombre }}@endif</td>
                                    <td>
                                        <a href="{{route('subjects.edit', $asignatura)}}" type="button" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></a>
                                        <form id="formBorrar{{$asignatura->id}}" class="d-inline" action="{{route('subjects.destroy', $asignatura)}}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-danger d-none"><i class="fas fa-trash"></i></button>
                                        </form>
                                        <button class="btn btn-danger" onclick=
                                        "Swal.fire({
                                            title: '¿Desea eliminar la asignatura \u201C{{$asignatura->nombre}}\u201D?',
                                            showConfirmButton: true,
                                            showCancelButton: true,
                                            confirmButtonText: 'Eliminar',
                                            cancelButtonText: `Cancelar`,
                                            }).then((result) => {
                                            if (result.isConfirmed) {
                                                document.getElementById('formBorrar{{$asignatura->id}}').submit();
                                            }
                                        })"
                                        ><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                <div class="d-flex">
                    {!! $asignaturas->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
