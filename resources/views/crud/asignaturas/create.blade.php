@extends('layouts.app')

@section('content')
<form action="{{ route('subjects.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Nueva asignatura</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('subjects.index')}}" type="button" class="btn btn-secondary mb-3"><i class="fa-solid fa-arrow-left"></i></a></div>
                    <div class="float-end">
                        <button type="submit" class="btn btn-primary me-1"><i class="fa-solid fa-plus"></i> Crear</button>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12 d-flex justify-content-center align-items-center">
                        <div style="font-size: large;">
                            <div class="mb-3">
                                <label for="nombre" class="form-label"><b>Nombre:</b></label>
                                <input type="text" class="form-control" name="nombre">
                                @error('nombre')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="profesor" class="form-label"><b>Profesor:</b></label>
                                <select id="profesor" name="profesor" class="form-select" aria-label="Default select example">
                                    <option selected="true" value="">Sin profesor</option>
                                    @foreach ($teachers as $teacher)
                                        <option value="{{ $teacher->id }}">{{ $teacher->nombre }}</option>
                                    @endforeach
                                </select>
                                @error('profesor')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
