@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="pb-1"><i class="fa-solid fa-chalkboard-user"></i> Detalles del profesor</h1>
                <div class="clearfix">
                    <div class="float-start"><a href="{{route('subjects.index')}}" type="button" class="btn btn-secondary mb-3"><i class="fa-solid fa-arrow-left"></i></a></div>
                    <div class="float-end">
                        <a href="{{route('subjects.edit', $subject)}}" class="btn btn-warning me-1"><i class="fa-solid fa-pen-to-square"></i> Editar</a>
                        <form id="formBorrar" class="d-inline" action="{{route('subjects.destroy', $subject)}}" method="POST">
                            @csrf
                            @method("DELETE")
                            <button type="submit" class="btn btn-danger d-none"><i class="fas fa-trash"></i></button>
                        </form>
                            <button class="btn btn-danger" onclick=
                            "Swal.fire({
                                title: '¿Desea eliminar la asignatura \u201C{{$subject->nombre}}\u201D?',
                                showConfirmButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Eliminar',
                                cancelButtonText: `Cancelar`,
                                }).then((result) => {
                                if (result.isConfirmed) {
                                    document.getElementById('formBorrar').submit();
                                }
                            })"
                            ><i class="fas fa-trash"></i> Eliminar</button>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-6 d-flex justify-content-center align-items-center mb-5">
                        <div>
                            @if (!empty($subject->profesor_id))
                                @if ($subject->teacher->avatar === null)
                                    <img width="265px" height="330px" src="https://i.ibb.co/Cmp8Xr7/nopic.png"/>
                                @else
                                    <img width="265px" height="330px" src="{{ Storage::url($subject->teacher->avatar) }}" style="object-fit: cover;"/>
                                @endif
                            @else
                                <img width="265px" height="330px" src="https://i.ibb.co/Cmp8Xr7/nopic.png"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <div style="font-size: large;">
                            <p><b>Nombre:</b> {{$subject->nombre}}</p>
                            <p><b>Profesor:</b>@if (empty($subject->profesor_id)) @else {{$subject->teacher->nombre}} @endif</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
